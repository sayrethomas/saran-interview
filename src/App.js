import logo from './logo.svg';
import './App.css';
import {Component} from "react";

export default class App extends Component {
	constructor() {
		super();

		this.state = {
		  searchTerm: '',
			foundItems: [],
            page: 1,
			totalResults: 0
		}
	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		const {page, searchTerm, foundItems} = this.state;

		if(page !== prevState.page || searchTerm !== prevState.searchTerm) {
		  if(searchTerm) {
			fetch(`http://www.omdbapi.com/?apikey=6f02c9a3&s=${searchTerm}&page=${page}`)
				.then(data => data.json())
				.then((data) => {
				  const newResults = [...foundItems, ...data.Search];
				  console.log(data);
				  const totalResults = parseInt(data.totalResults);
					this.setState({searchTerm: searchTerm, foundItems: newResults, totalResults: totalResults})
				})
          }
        }
	}

	//Form submission should set the search term
	populateSearch(e) {
	  e.preventDefault();
	  const form = e.currentTarget;
	  const searchTerm = form.querySelector('#searchTerm').value;

	  this.setState({searchTerm: searchTerm, page: 1, foundItems: [], totalResults: 0});
    }

	getMoreResults = () => {
	  const {page} = this.state;

	  const updatedPage = page + 1;

	  this.setState({page: updatedPage});
    }

	showGetMoreButton() {
	  //We should show get more if we have already shown first page of results or max results have not been reached
		const {page, searchTerm, foundItems, totalResults} = this.state;
      console.log(foundItems.length, totalResults);
		return !!foundItems.length && foundItems.length < totalResults;
    }

	render() {
	  const {searchTerm, foundItems} = this.state;

		return (
			<div className="App">
				<form onSubmit={(e) => this.populateSearch(e)}>
                  <input type={"text"} required={true} id={"searchTerm"} />
                  <button>Search</button>
                </form>
              <div className={"foundItemsWrap"}>
                <ul>
                    {
						foundItems.map((item) => {
                            return <li key={item.imdbID} className={`itemsListing ${item.Type === 'game' ? "gameClass" : "movieClass"}`}>
                              <img src={item.Poster} />
                              <div className={"infoWrap"}>
                                <h3>{item.Title}</h3>
								  <p>{item.Year}</p>
								  <p>{item.Type}</p>
                              </div>
                            </li>
                        })
                    }
                </ul>
              </div>
                <div className={"paginationWrap"}>
                    {
                      this.showGetMoreButton() &&
                      <button onClick={this.getMoreResults}>Get More</button>
                    }
                </div>
			</div>
		);
	}
}
